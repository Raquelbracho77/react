console.log('OK');

articleHeading = React.createElement(
	'h2',
	null,
	'My first blog post!'
	);

const paragraph = React.createElement(
	'p', 
	{classname: 'text'}
	'This is the content of my post.'
	);

const article = React.createElement(
	'article', 
	null, 
	articleHeading,
	paragraph);

const mainHeading = React.createElement(
	'h1',
	null,
	'My Blog'
	);

const main = React.createElement(
	'main',
	{id:'main'},
	mainHeading
	article
	
	);

ReactDOM.render(main, document.getElementById('root'));