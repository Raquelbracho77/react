const express = require('express');
const bodyParser = require('body-parser');

/*
	 command: 
	 $ node server
*/

const app = express();
app.use(bodyParser.json());
const port = 5000;

let records = [
	{
		id: 1,
		recordName : 'React Rave',
		artistName: 'The developers',
		description: 'Lorem Ipsum',
	},
	{
		id: 2,
		recordName : 'Building an App',
		artistName: 'The Components',
		description: 'Sounds of the future.',
	},
];

app.get('/api/records', (req, res) => {
	res.send(records);
});

app.listen(port, () => 
	console.log(`Server listening on port ${port}`)
);