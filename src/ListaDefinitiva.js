import React from 'react';

const SourceObj = {
	val1: 'Value 1',
	val2: 'Value 2',
	val3: 'Value 3',
	val4: 'Value 4',
};

const ListaDefinitiva = () => {
	return (
		<section>
			<h1>Ultimate Object Listing</h1>
			<ol>
				{Object.keys(SourceObj).map(key => (
					<li key={key}>{SourceObj[key]}</li>
				))}
			</ol>
		</section>
		);
};

export default ListaDefinitiva;