import React from 'react';
import {render} from 'react-dom';

console.log('OK');

const someValue = 5;

const calculate = (x,y) => x * y * someValue;

const paragraph = <h1>{calculate(10, 20)}</h1>;

ReactDOM.render(paragraph, document.getElementById('root'));