import {Fragment, useEffect, useState} from 'react';

const subscribe = () => {
	console.log("suscribed");
};

const unsuscribe = () => {
	console.log("unsuscribe");
};

const Counter = () => {
	const [counter, setCounter] = useState(0);
	
	useEffect(() =>{
		document.title = `Counter set to ${counter} | Ultimate Counter`;

	}, [counter]);
		
	const onCountClickHandler = () => {
		setCounter(c => c + 1);
	};

	return (
		<Fragment>
			<p>{counter}</p>
			<button type="button" onClick={onCountClickHandler}>
			Increment
			</button>
		</Fragment>
	);
};

export default Counter;