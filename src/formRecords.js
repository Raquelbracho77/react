import React, {useState} from 'react';
import Input from './input';

const initialEntryState = {
	recordName: '',
	artistName: '',
	description: '',
};

const Form = ( {onSubmit} ) => {
		const [entry, setEntry] = useState(initialEntryState);

		const onChangeHandler = e => {
			setEntry({
				...entry,
				[e.target.name]: e.target.value
			});
		};

		const onSubmitHandler = e => {
			e.preventDefault();
			console.log(entry);

			if(!entry.recordName || !entry.artistName){
				return;
			}
			onSubmit({...entry});
			setEntry(initialEntryState);
		}

	return(
	<form onSubmit={onSubmitHandler}>
		{/*<label htmlFor="recordName">Record Name</label>
		<input 
			id="recordName" 
			name="recordName"
			onChange={onChangeHandler}
			value={entry.recordName}
		/>*/}
		<Input 
			labelText="Record name"
			name="recordName"
			onChange={onChangeHandler}
			value={entry.recordName}
		/>
		<Input 
			labelText="Artist name"
			name="artistName"
			onChange={onChangeHandler}
			value={entry.artistName}
		/>
	
		<Input 
			type="textarea"
			labelText="Description"
			name="description"
			onChange={onChangeHandler}
			value={entry.description}
		/>
		<button type="submit">Add</button>
	</form>
	)
};

export default Form;