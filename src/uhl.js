import React, {useState, Fragment} from 'react';

const list = [
	// objeto array diccionario
	{id: 1, name:'The beach', topDestination: true},
	{id: 2, name:'The mountains',topDestination: false}, // destino q se mostrará
	{id: 3, name:'Vibrant cities', topDestination: true},
	{id: 4, name:'Roughing it', topDestination: false},
	{id: 5, name:'Ultimate survival', topDestination: false}, // destino que no se mostrará

];



const UHL = () => {
	const [showAll, setShowAll] = useState(true);

	return(
		<section>
			<h1>Ultimate Holiday Destinations</h1>

			{/*
				Comentario: filter() creará un nuevo array con todos los
				topDestination:true
			*/}
			{list.filter(item => showAll ? true : item.topDestination).
			map(item => <li key={item.id.toString()}>
			{item.name}</li>)}
			<button type="button"
			onClick={ () => {
				setShowAll(true);
			}}
			>
				Show all
			</button>
			<button type="button"
			onClick={ () => {
				setShowAll(false);
			}}
			>
				Show only top destinations
			</button>
		</section>
		);
};
	


export default UHL;