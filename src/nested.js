console.log('OK');

const articleHeading = React.createElement(
		'h2',
		null,
		'My first blog post'
	);

const paragraph = React.createElement(
		'p',
		{ className:'text' },
		'This is the content of my post'
	);

const article = React.createElement(
		'article',
		null,
		articleHeading,
		paragraph
	);

const mainHeading = React.createElement(
		'h1',
		null,
		'My Blog'
	);

const button = React.createElement(
	'button',
	{type: 'button'},
	'OK'
	);

const main = React.createElement(
		'main',
		{id: 'main'},
		mainHeading,
		article,
		button
	);

ReactDOM.render(main, document.getElementById('root'));