import React from 'react';
import {render} from 'react-dom';

console.log('OK');


const getText = name => `My name is ${name}`;

const container = <div>{getText('Raquel')}</div>;

ReactDOM.render(container, document.getElementById('root'));